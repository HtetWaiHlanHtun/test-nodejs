// const config=require('config');
const Mongoose=require('mongoose');
const Joi=require('Joi');
const express= require('express');
const users= require('./routes/users');
const user= require('./routes/account');
const login= require('./routes/login');
const app=express();

// if(!config.get('jwtPrivateKey'))
// {
// console.error('FATAL Error: jwtPrivateKey is not defined!');
// process.exit(1);
// }
app.use(express.json());

app.use('/auth/register',users);
app.use('/auth/login',login);
app.use('/account',user);
app.use('/account',user);




Mongoose.connect('mongodb://localhost/test')
        .then(()=>console.log('Connected to MongoDB....'))
        .catch(err=> console.err('Could not connect to MongoDB...'));


const port=process.env.PORT || 3000;
app.listen(port,()=>console.log(`Listening on port  ${port}.......`));

