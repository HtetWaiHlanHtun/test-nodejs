const bcrypt=require('bcrypt');
const _=require('lodash');
const {User,validate}= require('../models/user');
const mongoose= require('mongoose');
const express= require('express');
const router=express.Router();

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now())
    next()
  }); 
router.post('/',async(req,res)=>{
    const {error} =validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    let user=await User.findOne({email:req.body.email});
    if(user) return res.status(400).send('User already resgistered...');

    user=new User(_.pick(req.body,['_id','name','email','password','hometown']));
    const salt=await bcrypt.genSalt(10);
    user.password=await bcrypt.hash(user.password,salt);
    await user.save();
    res.send(_.pick(user,['_id','name','email','hometown']));
});

module.exports=router;