// const config=require('config');
const jwt=require('jsonwebtoken');
const bcrypt=require('bcrypt');
const _=require('lodash');
const Joi=require('Joi');
const {User,validate}= require('../models/user');
const mongoose= require('mongoose');
const express= require('express');
const router=express.Router();

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now())
    next()
  }); 
router.put('/',async(req,res)=>{
    const {error} =validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    let user=await User.findOne({email:req.body.email});
    if(!user) return res.status(400).send('Invalid email or password');

    const validPassword= bcrypt.compare(req.body.password,user.password);
    if(!validPassword) return res.status(400).send('Invalid email or password');
    
    // user=_.update(req.body,['_id','name','email','password','hometown'],user);
    user.name=req.body.name;
    user.hometown=req.body.hometown;
    
    //console.log(user);
    const salt=await bcrypt.genSalt(10);
    user.password=await bcrypt.hash(user.password,salt);
    await user.save();
    res.send(_.pick(user,['_id','name','email','hometown']));
    
});


router.delete('/',async(req,res)=>{
    const {error} =validateUser(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    let user=await User.findOne({email:req.body.email});
    if(!user) return res.status(400).send('Invalid email or password');

    const validPassword= bcrypt.compare(req.body.password,user.password);
    if(!validPassword) return res.status(400).send('Invalid email or password');
    
    let result=await User.findOneAndDelete({email:req.body.email});
    if(!result) return res.status(400).send('Cant delete');
    res.send(true);
    
});
function validateUser(req){
    const schema={ 
        email:Joi.string().min(5).max(255).required(),
        password:Joi.string().min(5).max(1024).required(),
        
    };
    return Joi.validate(req,schema);
}

module.exports=router;
